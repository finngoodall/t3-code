"""
Small program to plot the velocity profiles from T3
"""

# Get libraries
import matplotlib.pyplot as plt
import numpy as np
import math

## Part 1 - Plot original graphs

dh_freestream = 101 																							# freestream pitot tube reading [mm]

# Laminar flow
x_lam = [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.25,1.5,1.75,2.0,2.25,2.50,3.00,3.50,4.00,4.5,5.0]			# y [mm]
dh_lam = [0,0,0,2,4,7,10,13,16,20,30,40,50,60,67,74,84,92,96,97,98]												# pitot tube reading [mm]

vel_ratio_lam = [(i/dh_freestream)**0.5 for i in dh_lam]														# ratio u/U_infinity

# Turbulent flow
x_turb = [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.25,1.5,1.75,2.0,2.25,2.5,3.0,4.0,5.0,6.0,7.5,10.0,12.5,15.0,17.5,20.0]
dh_turb = [4,4,4,10,16,22,26,30,32,35,36,40,42,44,46,48,49,52,57,62,65,71,80,86,91,95,98]

vel_ratio_turb = [(i/dh_freestream)**0.5 for i in dh_turb]


# Plot
fig = plt.figure()
ax = fig.add_subplot(111)

ax.scatter(x_lam, vel_ratio_lam, c='b', label='Laminar Flow')
ax.scatter(x_turb, vel_ratio_turb, c='r', label='Turbulent Flow')
plt.legend(loc='lower right')
plt.title('Velocity Profiles')
plt.xlabel('y [mm]')
plt.ylabel('u/U_infinity [-]')

plt.show()



## Part 2 - Extrapolate backwards

# Get linear relationship; take relevant values up to 1.0mm
lam_linear = np.polyfit(x_lam[:10], vel_ratio_lam[:10], 1)
turb_linear = np.polyfit(x_turb[2:10], vel_ratio_turb[2:10], 1)

# Extrapolate backwards to find plate level
plate_lam = -lam_linear[1]/lam_linear[0]	
plate_turb = -turb_linear[1]/turb_linear[0]

print('The laminar plate is at {}mm and the turbulent plate is at {}mm'.format(round(plate_lam,3), round(plate_turb,3)))



## Part 3 - Redraw graphs

# Adjust plate values to fit through origin
x_lam = [i-plate_lam for i in x_lam]
x_turb = [i+plate_turb for i in x_turb]

for i in x_lam:
	if i < 0:
		max_index_lam = x_lam.index(i)
for i in x_turb:
	if i < 0:
		max_index_turb = x_turb.index(i)

x_lam, vel_ratio_lam = x_lam[max_index_lam+1:], vel_ratio_lam[max_index_lam+1:]
x_turb, vel_ratio_turb = x_turb[max_index_turb+1:], vel_ratio_turb[max_index_turb+1:]

# Replot graphs
fig = plt.figure()
ax = fig.add_subplot(111)

ax.scatter(x_lam, vel_ratio_lam, c='b', label='Laminar Flow')
ax.scatter(x_turb, vel_ratio_turb, c='r', label='Turbulent Flow')
plt.legend(loc='lower right')
plt.title('Velocity Profiles (Updated y values)')
plt.xlabel('y [mm]')
plt.ylabel('u/U_infinity [-]')

plt.show()



## Part 4 - Find shear force from rate of momentum lost

# Plot graphs of (1-u/U)(u/U)
func_lam = [i*(1-i) for i in vel_ratio_lam]
func_turb = [i*(1-i) for i in vel_ratio_turb]

plt.plot(x_lam, func_lam, c='b', label='Laminar Flow')
plt.plot(x_turb, func_turb, c='r', label='Turbulent Flow')
plt.legend(loc='upper right')
plt.title('Plot of (1-u/U)(u/U)')
plt.xlabel('y [mm]')
plt.ylabel('Value of Function [-]')

plt.show()

# Integrate graphs numerically using trapezoidal rule
I_lam, I_turb = 0,0
for i in range(len(func_lam)-1):
	I_lam += 0.5*(func_lam[i]+func_lam[i+1])*(x_lam[i+1]-x_lam[i])
for i in range(len(func_turb)-1):
	I_turb += 0.5*(func_turb[i]+func_turb[i+1])*(x_turb[i+1]-x_turb[i])

print('The integrals under the graph are {}mm for the laminar flow and {}mm for the turbulent flow'.format(round(I_lam, 3), round(I_turb, 3)))

# Multiply by rho*U^2 to get dM/dt
U_freestream = 10.83						# Freestream velocity [m/s]
density_air = 1.2							# Density of air in wind tunnel [kg/m^3]

I_lam *= density_air * U_freestream**2 / 1000
I_turb *= density_air * U_freestream**2 / 1000

print('The loss in momentum flux across the plate is {}N/m for the laminar flow and {}N/m for the turbulent flow'.format(round(I_lam, 3), round(I_turb, 3)))



## Part 5 - Compare shear force per unit width to initial gradient

# Get initial gradient from graphs - 'linear' region found earlier in part 2
viscosity_air = 1.75e-5

shear_lam = viscosity_air*U_freestream*1000*lam_linear[0]
shear_turb = viscosity_air*U_freestream*1000*turb_linear[0]

print('The estimated skin friction drag from the initial gradient is {}N/m^2 for the laminar flow and {}N/m^2 for the turbulent flow'.format(round(shear_lam, 3), round(shear_turb, 3)))

# Estimate skin friction test from momentum flux loss
#shear_lam_graphical = I_lam * 
#shear_turb_graphical = I_turb * 